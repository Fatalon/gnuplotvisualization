set cbrange [0.9:1]
set palette defined (1 '#ce4cfd')
set style line 1 lc rgb '#b90046' lt 1 lw 0.5
set pm3d depthorder hidden3d
set pm3d implicit
unset hidden3d
splot 'workpiece.txt' u 1:2:3:(1) w l ls 1
