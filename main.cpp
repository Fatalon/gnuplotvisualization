#include <iostream>
#define GNUPLOT_ENABLE_PTY
#include "gnuplot-iostream.h"

namespace xyz {
    template<typename T>
    struct MyTriple {
        MyTriple(
                T _x,
                T _y,
                T _z
        ) : x(_x), y(_y), z(_z) {}

        T x, y, z;
    };
}
template<typename T>
struct gnuplotio::TextSender<xyz::MyTriple<T> > {
    static void send(std::ostream &stream, const xyz::MyTriple<T> &v) {
        TextSender<T>::send(stream, v.x);
        stream << " ";
        TextSender<T>::send(stream, v.y);
        stream << " ";
        TextSender<T>::send(stream, v.z);
    }
};


int nmain() {
    Gnuplot gp;


    using xyz::MyTriple;
    gp << "set xrange [-5:5]\nset yrange [-5:5]\nset zrange[-5:5]\n";

    for (int index{}; index < 5; ++index) {
        std::string text =
                "set label " + std::to_string(index+1) + " \"\" at " + std::to_string(index) + "," +
                std::to_string(index) + "," +
                std::to_string(index) +
                " point pointtype 1\n";
        gp << text;
    }
    std::vector<MyTriple<int>> triples{};
    MyTriple<int> triple{1, 2, 3};
    triples.push_back(triple);
    gp << "splot '-' with vectors title 'pts_A', '-' with vectors title 'pts_B'\n";
    gp.send(triples);
    //gp.send1d(pts_A);
    //gp.send1d(std::vector<std::vector<double>>{pts_B_x, pts_B_y, pts_B_dx, pts_B_dy});
    //gp.send2d(point3d);
    std::cout << "Hello, World!" << std::endl;
    return 0;
}